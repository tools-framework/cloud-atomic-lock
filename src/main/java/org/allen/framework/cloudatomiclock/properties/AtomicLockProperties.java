package org.allen.framework.cloudatomiclock.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: Allen
 * @Date: Created in 11:08 2019-10-11
 * @Description:
 */
@Data
@Component
@ConfigurationProperties(prefix = "cloud.atomic.lock")
public class AtomicLockProperties {

    private String url;
    private String username;
    private String password;
    private long timeout;

}
