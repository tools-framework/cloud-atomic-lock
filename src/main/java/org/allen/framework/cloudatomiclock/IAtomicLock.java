package org.allen.framework.cloudatomiclock;

/**
 * @Author: Allen
 * @Date: Created in 11:05 2019-10-11
 * @Description:
 */
public interface IAtomicLock {

    void lock(String key) throws InterruptedException, Exception;

    void unlock(String key);

}
