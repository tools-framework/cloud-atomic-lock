package org.allen.framework.cloudatomiclock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * @Author: Allen
 * @Date: Created in 09:47 2019-10-12
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class AtomicLockTest {

    @Autowired
    private DemoService demoService;

    @Test
    public void lockTest() throws InterruptedException {

        final CountDownLatch countDownLatch = new CountDownLatch(10);
        ExecutorService executorService = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r);
            }
        });
        for (int i = 0; i < 10; i++) {
            countDownLatch.countDown();
            executorService.submit(new Runnable() {
                public void run() {
                    for (int i = 0; i < 100; i++) {
                        demoService.handle(i);
                    }
                }
            });
        }
        countDownLatch.await();
        Thread.currentThread().join();
        executorService.shutdown();
    }


}
