package org.allen.framework.cloudatomiclock;

import org.allen.framework.cloudatomiclock.annotation.NeedAtomicLock;
import org.allen.framework.cloudatomiclock.properties.AtomicLockProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * @Author: Allen
 * @Date: Created in 09:55 2019-10-12
 * @Description:
 */
@Service
public class DemoService {

    @Bean
    public AtomicLockProperties atomicLockProperties() {
        AtomicLockProperties atomicLockProperties = new AtomicLockProperties();
        atomicLockProperties.setUrl("127.0.0.1:2181");
        atomicLockProperties.setTimeout(1000);
        return atomicLockProperties;
    }

    @NeedAtomicLock("demolock")
    public void handle(int index) {
        System.out.println("执行服务:" + index);
    }

}
